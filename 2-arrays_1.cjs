const data = require("./data.cjs");
// 1. Write a function that takes in a genre and returns an array of movie titles that belong to that genre, sorted in descending order of rating.

function getMoviesByGenre(data, genreName) {
  return data
    .reduce((names, movie) => {
      if (movie.genre.includes(genreName)) {
        names.push(movie.title);
      }
      return names;
    }, [])
    .sort((a, b) => b.rating - a.rating);
}

console.log(getMoviesByGenre(data, "Action"));

// expected output:
// [
//   "The Dark Knight",
//   "Inception",
//   "The Matrix",
//   "The Lord of the Rings: The Return of the King",
//   "The Avengers",
//   "Terminator 2: Judgment Day"
// ]

// 2. Write a function that takes in an actor's name and returns an array of movie titles that the actor appeared in, sorted by year of release.

function getMoviesByActor(data, actorName) {
  return data
    .reduce((names, movie) => {
      if (movie.cast.includes(actorName)) {
        names.push(movie.title);
      }
      return names;
    }, [])
    .sort((a, b) => b.year - a.year);
}

console.log(getMoviesByActor(data, "Leonardo DiCaprio"));

// expected output:
// [
//   "Titanic",
//   "The Departed",
//   "The Wolf of Wall Street",
//   "The Revenant"
// ]

// 3. Write a function that takes in a director's name and returns an object with the director's name as -
// - a key and an array of movie titles that the director directed as the value.

function getMoviesByDirector(data, directorName) {
  return data.reduce((names, movie) => {
    if (movie.director === directorName) {
      if (!names[directorName]) {
        names[directorName] = [];
      }
      names[directorName].push(movie.title);
    }
    return names;
  }, {});
}

console.log(getMoviesByDirector(data, "Christopher Nolan"));

// expected output:
// {
//   "Christopher Nolan": [
//     "The Dark Knight",
//     "Inception",
//     "Interstellar",
//     "Dunkirk",
//     "Memento"
//   ]
// }

// 4. Write a function that takes in a year and returns an object with the year as a key and an array of movie
// titles released in that year as the value, sorted in descending order of rating.

function getMoviesByYear(data, ryear) {
  return data.reduce((names, movie) => {
    if (movie.year == ryear) {
      if (!names[ryear]) {
        names[ryear] = [];
      }
      names[ryear].push(movie.title);
    }
    return names;
  }, {});
}

console.log(getMoviesByYear(data, 1994));

// expected output:
// {
//   1994: [
//     "The Shawshank Redemption",
//     "Pulp Fiction",
//     "Forrest Gump"
//   ]
// }

// 5. Write a function that returns an array of objects, where each object contains a unique combination of director and genre.
// Each object should have a "director" key and a "genre" key, and the values of these keys should be arrays of movie titles that
//  match the combination of director and genre. The movie titles should be sorted in descending order of rating.

// getMoviesByDirectorAndGenre();

// expected output:
// [
//   {
//     director: "Christopher Nolan",
//     genre: "Action",
//     movies: ["The Dark Knight"]
//   },
//   {
//     director: "Christopher Nolan",
//     genre: "Thriller",
//     movies: ["Inception"]
//   },
//   {
//     director: "Christopher Nolan",
//     genre: "Sci-Fi",
//     movies: ["Interstellar"]
//   },
//   {
//     director: "Steven Spielberg",
//     genre: "Action",
//     movies: ["Jurassic Park"]
//   },
//   {
//     director: "Steven Spielberg",
//     genre: "Drama",
//     movies: ["Schindler's List"]
//   },
//   {
//     director: "Steven Spielberg",
//     genre: "Adventure",
//     movies: ["Indiana Jones and the Raiders of the Lost Ark"]
//   }
// ]
